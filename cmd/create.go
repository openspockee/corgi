package cmd

import (
	"fmt"
	"os"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
	"spockee.com/corgi/utils"
)

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create db service",
	Long: `
This is used to create db service from template.	
	`,
	Run: runCreate,
}

type DatabaseService struct {
	ServiceName  string
	User         string `yaml:"user"`
	Password     string `yaml:"password"`
	DatabaseName string `yaml:"databaseName"`
	Port         int    `yaml:"port"`
}

type FilenameForService struct {
	Name     string
	Template string
}

func runCreate(cmd *cobra.Command, args []string) {
	addFolderStringToGitignore(utils.RootDbServicesFolder)

	services, err := getServicesFromConfigFile()
	if err != nil {
		fmt.Printf("couldn't get services config, error: %s\n", err)
	}

	if len(services) == 0 {
		fmt.Println(`
No services info provided.
Please provided them in corgi-compose.yml file`)
		return
	}

	filesToCreate := []FilenameForService{
		{"docker-compose.yml", dockerComposeTemplate},
		{"Makefile", makefileTemplate},
	}

	for _, service := range services {
		for _, file := range filesToCreate {
			err := createFileFromTemplate(
				service,
				file.Name,
				file.Template,
			)

			if err != nil {
				fmt.Printf(
					"error creating %s for service %s, error: %s",
					file.Name,
					service.ServiceName,
					err,
				)
				break
			}
		}
		fmt.Printf("Db service %s was successfully created\n", service.ServiceName)
	}
}

func getServicesFromConfigFile() ([]DatabaseService, error) {
	composeFileName := "corgi-compose.yml"

	file, err := os.ReadFile(composeFileName)
	if err != nil {
		return nil, fmt.Errorf("couldn't read %s", composeFileName)
	}

	data := make(map[string]map[string]DatabaseService)

	err = yaml.Unmarshal(file, &data)

	if err != nil {
		return nil, fmt.Errorf("couldn't unmarshal %s", composeFileName)
	}

	var services []DatabaseService
	for indexName, service := range data[utils.RootDbServicesFolder] {
		services = append(services, DatabaseService{
			ServiceName:  indexName,
			DatabaseName: service.DatabaseName,
			User:         service.User,
			Password:     service.Password,
			Port:         service.Port,
		})
	}

	if len(services) == 0 {
		return nil, fmt.Errorf("no service is found in %s", composeFileName)
	}

	return services, nil
}

func addFolderStringToGitignore(folder string) error {
	f, err := os.OpenFile(
		".gitignore",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0644,
	)
	if err != nil {
		return fmt.Errorf("couldn't open .gitignore file, error: %s", err)
	}
	defer f.Close()

	content, err := os.ReadFile(".gitignore")
	if err != nil {
		return fmt.Errorf("couldn't read .gitignore file, error: %s", err)
	}

	if !strings.Contains(string(content), folder) {
		_, err := f.WriteString(fmt.Sprintf(`
%s/
corgi-compose.yml

`, folder))
		if err != nil {
			return fmt.Errorf(
				"couldn't add %s to .gitignore, error: %s",
				folder,
				err,
			)
		}
		defer f.Close()
	}
	return nil
}

func createFileFromTemplate(
	dbService DatabaseService,
	fileName string,
	fileTemplate string,
) error {
	path := fmt.Sprintf(
		"%s/%s",
		utils.RootDbServicesFolder,
		dbService.ServiceName,
	)

	err := os.MkdirAll(path, os.ModePerm)

	if err != nil {
		return fmt.Errorf("error of creating %s, error: %s", path, err)
	}

	dockerComposeFile := fmt.Sprintf("%s/%s", path, fileName)

	f, err := os.Create(dockerComposeFile)

	if err != nil {
		return fmt.Errorf("error of creating %s, error: %s", dockerComposeFile, err)
	}

	defer f.Close()

	tmp := template.Must(template.New("simple").Parse(fileTemplate))

	err = tmp.Execute(f, dbService)

	if err != nil {
		return fmt.Errorf(
			"error of creating template %s, error: %s",
			dockerComposeFile,
			err,
		)
	}
	return nil
}

var dockerComposeTemplate = `version: "3.8"

services:
  postgres:
    image: postgres:11.5-alpine
    container_name: postgres-{{.ServiceName}}
    logging:
      driver: none
    environment:
      - POSTGRES_USER={{.User}}
      - POSTGRES_PASSWORD={{.Password}}
      - POSTGRES_DB={{.DatabaseName}}
    ports:
      - "{{.Port}}:5432"
`

var makefileTemplate = `up:
	docker compose up -d
down:
	docker compose down    
stop:
	docker stop postgres-{{.ServiceName}}
id:
	docker ps -aqf "name=postgres-{{.ServiceName}}" | awk '{print $1}'
seed:
	cat dump.sql | docker exec -i $(c)  psql -U {{.User}} -d {{.DatabaseName}}
help:
	make -qpRr | egrep -e '^[a-z].*:$$' | sed -e 's~:~~g' | sort

.PHONY: up down stop id seed help
`

func init() {
	rootCmd.AddCommand(createCmd)
}
